package model;

public class Film {
	
	private int id;
	private String title;
	private String director;
	private int releasedate;
	private int featurelength;
	private int genre_id;
	
	
	public Film(String title, String director, int releasedate, int featurelength) {
		this.title = title;
		this.director = director;
		this.releasedate = releasedate;
		this.featurelength = featurelength;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDirector() {
		return director;
	}


	public void setDirector(String director) {
		this.director = director;
	}


	public int getReleasedate() {
		return releasedate;
	}


	public void setReleasedate(int releasedate) {
		this.releasedate = releasedate;
	}


	public int getFeaturelength() {
		return featurelength;
	}


	public void setFeaturelength(int featurelength) {
		this.featurelength = featurelength;
	}


	public int getGenre_id() {
		return genre_id;
	}


	public void setGenre_id(int genre_id) {
		this.genre_id = genre_id;
	}
}
