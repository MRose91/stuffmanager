package model;

public class Music {

	private int id;
	private String title;
	private String artist;
	private int releasedate;
	private int tracklength;
	private int genre_id;
	
	public Music(String title, String artist, int releasedate, int tracklength){
		this.title = title;
		this.artist = artist;
		this.releasedate = releasedate;
		this.tracklength = tracklength;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public int getReleasedate() {
		return releasedate;
	}

	public void setReleasedate(int releasedate) {
		this.releasedate = releasedate;
	}

	public int getTracklength() {
		return tracklength;
	}

	public void setTracklength(int tracklength) {
		this.tracklength = tracklength;
	}

	public int getGenre_id() {
		return genre_id;
	}

	public void setGenre_id(int genre_id) {
		this.genre_id = genre_id;
	}
}
