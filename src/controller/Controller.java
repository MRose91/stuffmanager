package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.sun.javafx.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class Controller implements Initializable {

	private ObservableList<String> categoryList = FXCollections.observableArrayList("B�cher", "Musik", "Filme");

	private Tab tab;

	private Pane pane;

	@FXML
	private TabPane tp_tabpane;

	@FXML
	private ComboBox<String> cb_category;

	@FXML
	private Label lb_label1;

	@FXML
	private Label lb_label2;

	@FXML
	private Label lb_label3;

	@FXML
	private Label lb_label4;

	@FXML
	private TableView<?> tbl_table;

	@FXML
	private Button btn_delete;

	@FXML
	private Button btn_save;

	@FXML
	private TextField tx_text1;

	@FXML
	private TextField tx_text2;

	@FXML
	private TextField tx_text3;

	@FXML
	private TextField tx_text4;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		cb_category.setItems(categoryList);

		cb_category.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String selectedItem = cb_category.getSelectionModel().getSelectedItem();

				switch (selectedItem) {

				case "B�cher":

					pane = new Pane();
//						pane = FXMLLoader.load(getClass().getResource("/view/Tab.fxml"));
					tab = new Tab("B�cher");
					tab.setContent(pane);
					tp_tabpane.getTabs().add(tab);

					break;

				case "Musik":

					tab = new Tab("Musik");
					tp_tabpane.getTabs().add(tab);
					break;

				case "Filme":

					tab = new Tab("Filme");
					tp_tabpane.getTabs().add(tab);
					break;

				}

			}
		});

	}

}
